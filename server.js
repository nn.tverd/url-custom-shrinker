const express = require("express");
const mongoose = require("mongoose");
const ShortUrl = require("./models/shortUrl.js");

mongoose.connect(
    "mongodb+srv://nikolay:7N94jcJ4Aajhcs5@cluster0.61coo.mongodb.net/shrink?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);
// nikolay
// 7N94jcJ4Aajhcs5

const app = express();

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));

app.get("/", async (req, res) => {
    // const shortUrls = await ShortUrl.find();
    // console.log(shortUrls);
    res.render("cloak");
    // res.render("index");
});

app.get("/q", async (req, res) => {
    const shortUrls = await ShortUrl.find();
    console.log(shortUrls);
    res.render("index", { shortUrls: shortUrls });
    // res.render("index");
});

app.post("/shortUrls", async (req, res) => {
    await ShortUrl.create({ full: req.body.fullUrl, comment:req.body.comment });
    res.redirect("/q");
});

app.get("/:shortUrl", async (req, res) => {
    const shortUrl = await ShortUrl.findOne({ short: req.params.shortUrl });
    if (shortUrl == null) res.sendStatus(404);

    shortUrl.clicks++;
    shortUrl.save();

    res.redirect(shortUrl.full);
});

app.listen(process.env.PORT || 8080);
